# Инструкия по установке и запуске проекта TETRA

### Установка програмного обеспечения  

* [Java: 1.7.80](https://www.oracle.com)
Прописать в переменные среды путь к JDK:

![](img/JAVA_HOME.jpg)

* [PostgreSQL 9.5 и pgAdmin4](https://www.postgresql.org)(Чтобы накатить данные в БД.pgAdmin3 такой функции не имеет)
Создать базы данных как на скрине + создать роль `tetra_dev`:

   ![](img/bd-name.jpg)
   
* [Tomcat 7](https://tomcat.apache.org)
*	[Git](https://git-scm.com)
*	[Мaven 3.3.9](https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.3.9/)
	Прописать путь в переменные среды:

	![](img/m2.jpg)
	
	После передать переменные %M2%;%JAVA_HOME%\bin; в переменную PATH.
	Проверить в командной строке подключение Maven командой `mvn -version`
	Должен появиться такой результат:
	
	![](img/maven-version.jpg)
	
*	[OpenVPN](https://openvpn.net/)
	* Подложить файл с конфигурацией (пароль для zip profitsoft) в папку OpenVPN\config\
	* Запускать OpenVpn только с админскими правами. Проверить коннект и видимость git.strikersoft.com
	* Прописать в файл C:\Windows\System32\drivers\etc\hosts:
		  	 192.168.111.12 git.strikersoft.com
			 192.168.111.9 jenkins.strikersoft.com
*	Jboss-fuse-6.1.0.redhat-379

### <a name="ssh"></a>Добавление SSH ключа
  Ключ должен быть сгенерирован с помощью `ssh-keygen`.
  Если ключ уже есть по указанному пути, то можно использовать его:
	`C:\Users\<Имя пользователя>\.ssh\id_rsa.pub`
	Необходимо добавить в переменные среды
	`HOME=C:\Users\<Имя пользователя>`
	Генератор ключа должен установиться вместе с GIT для Windows.
	У меня он находится тут:
	`C:\Program Files\Git\usr\bin\ssh-keygen.exe`
	Команда для генерации:
	```
	ssh-keygen -t rsa -C "your.email@example.com" -b 4096
	```
	При генерации все вопросы пропускаем, нажимая enter. 
	В таком случае ключ генерится в стандартном месте,
	гит его сам найдет, и без пароля.  Если ввести пароль
	к ключу то придется вводить его для выполнения каждой команды в терминале гита.
	После того, как ключ сгенерирован необходимо добавить его на сервер в свой профиль.
	Для этого после аутентификации в нашем gitLab переходим по ссылке:
	http://git.strikersoft.com/
	Открыть в блокноте содержимое файла:
	`C:\Users\<Имя пользователя>\.ssh\id_rsa.pub`
	и вставить в поле Key на интерфейсе GitLab.

### Клонируем проект

В гите на страничке самого проекта будет ssh url (обязательно SSH, не http, иначе придется каждый раз вводить свой пароль к гит):
![](img/ssh.jpg)
В `cmd` выполнить команду, после чего проект скачается в каталог, в котором была выполнена команда:
`git clone git@git.strikersoft.com:tetra/tetra-backend.git`

### Конфигурация

  #### Tetra-backend

* Импортируем проект как `Maven` проект. Обязательно убедиться что подтянулись все зависимости и был успешно собран.
Собрать проект камондой `install`.
![](img/maven-install.jpg)
* Добавляем Remove config:
![](img/Remote.jpg)
* Конфигурация Jboss-fuse:
	В корневом файле меняем содержимое этих файлов:
	* [fuse_delete_data_&_lock.bat](/git/additionalFile/fuse_start_debug_install.txt)
	* [fuse_start_debug.bat](/git/additionalFile/fuse_start_debug.xml)
	* [fuse_start_debug_install.bat](/git/additionalFile/fuse_start_debug_install.txt)

	Переходим в папку \etc содаем файл [com.strikersoft.tetra.cfg'](/git/additionalFile/com.strikersoft.tetra.json).
	Меняем на свои данные.
	Запускаем jboss-fuse с помощью `fuse_start_debug_install.bat`

#### Tetra-prism-web

* Импортируем проект как `Maven` проект.
		Создаем Tomcat config:

![](img/tomcat-conf.jpg)
		
* Создаем файл [tetra-prism.properties](/git/additionalFile/tetra-prism.txt) в папке c Tomcat\conf
